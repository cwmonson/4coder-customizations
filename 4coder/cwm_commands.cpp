
#include "4coder_helper/4coder_helper.h"
#include "4coder_helper/4coder_long_seek.h"

#define right 1
#define left 0

static void
cwm_first_boundary_seek(Application_Links *app, int32_t seek_type){
    uint32_t access = AccessProtected;        
    View_Summary view = get_active_view(app, access);
    Buffer_Summary buffer = get_buffer(app, view.buffer_id, access);
    
    char c = 0;
    int32_t pos = view.cursor.pos;
    if(seek_type == left)
        pos--;
    
    if(buffer_read_range(app, &buffer, pos, pos+1, &c)){
        pos = buffer_boundary_seek(app, &buffer, view.cursor.pos, seek_type, BoundaryToken | BoundaryWhitespace |  BoundaryAlphanumeric | BoundaryCamelCase);
        view_set_cursor(app, &view, seek_pos(pos), true);
        if(char_is_whitespace(c)){
            //hacky reverse search to correct greedy seek!
            if(seek_type == left) exec_command(app,seek_white_or_token_right);
            else exec_command(app,seek_white_or_token_left);
        }
    }
}



CUSTOM_COMMAND_SIG(cwm_seek_boundary_right){
    cwm_first_boundary_seek(app, right);
}

CUSTOM_COMMAND_SIG(cwm_seek_boundary_left){
    cwm_first_boundary_seek(app, left); 
}

#undef right
#undef left

CUSTOM_COMMAND_SIG(cwm_delete_to_boundary_left){
    uint32_t access = AccessOpen;
    
    View_Summary view = get_active_view(app, access);
    Buffer_Summary buffer = get_buffer(app, view.buffer_id, access);
    
    if (buffer.exists){
        int32_t pos2 = 0, pos1 = 0;
        
        pos2 = view.cursor.pos;
        exec_command(app, cwm_seek_boundary_left);
        refresh_view(app, &view);
        pos1 = view.cursor.pos;
        
        buffer_replace_range(app, &buffer, pos1, pos2, 0, 0);
    }
}

CUSTOM_COMMAND_SIG(cwm_delete_to_boundary_right){
    uint32_t access = AccessOpen;
    
    View_Summary view = get_active_view(app, access);
    Buffer_Summary buffer = get_buffer(app, view.buffer_id, access);
    
    if (buffer.exists){
        int32_t pos2 = 0, pos1 = 0;
        
        pos1 = view.cursor.pos;
        exec_command(app, cwm_seek_boundary_right);
        refresh_view(app, &view);
        pos2 = view.cursor.pos;
        
        buffer_replace_range(app, &buffer, pos1, pos2, 0, 0);
    }
}


CUSTOM_COMMAND_SIG(cwm_mark_line){
    exec_command(app, seek_beginning_of_line);
    exec_command(app, set_mark);
    exec_command(app, move_down);
    exec_command(app, seek_beginning_of_line);
}



CUSTOM_COMMAND_SIG(cwm_cut_line){
    exec_command(app, cwm_mark_line);
    exec_command(app, cut);
}


CUSTOM_COMMAND_SIG(cwm_copy){
    uint32_t access = AccessProtected;
    View_Summary view = get_active_view(app, access);
    Range range = get_range(&view);
    if(range.min == range.max){
        exec_command(app, cwm_mark_line);
        exec_command(app, copy);
        exec_command(app, move_up);
    }else{
        exec_command(app, copy);
    }
}

CUSTOM_COMMAND_SIG(cwm_cut){
    uint32_t access = AccessOpen;
    View_Summary view = get_active_view(app, access);
    Range range = get_range(&view);
    if(range.min == range.max){
        exec_command(app, cwm_mark_line);
    }
    exec_command(app, cut);
}

CUSTOM_COMMAND_SIG(cwm_seek_file_start){
    uint32_t access = AccessProtected;
    
    View_Summary view = get_active_view(app, access);
    Buffer_Summary buffer = get_buffer(app, view.buffer_id, access);
    view_set_cursor(app, &view, seek_pos(0), false);
}

CUSTOM_COMMAND_SIG(cwm_seek_file_end){
    uint32_t access = AccessProtected;
    
    View_Summary view = get_active_view(app, access);
    Buffer_Summary buffer = get_buffer(app, view.buffer_id, access);
    view_set_cursor(app, &view, seek_pos(buffer.size), false);
}

CUSTOM_COMMAND_SIG(cwm_copy_theme_to_clipboard){
    const char* style_tag_names[] = {
        "Stag_Bar",
        "Stag_Bar_Active",
        "Stag_Base",
        "Stag_Pop1",
        "Stag_Pop2",
        "Stag_Back",
        "Stag_Margin",
        "Stag_Margin_Hover",
        "Stag_Margin_Active",
        "Stag_Cursor",
        "Stag_At_Cursor",
        "Stag_Highlight",
        "Stag_At_Highlight",
        "Stag_Mark",
        "Stag_Default",
        "Stag_Comment",
        "Stag_Keyword",
        "Stag_Str_Constant",
        "Stag_Char_Constant",
        "Stag_Int_Constant",
        "Stag_Float_Constant",
        "Stag_Bool_Constant",
        "Stag_Preproc",
        "Stag_Include",
        "Stag_Special_Character",
        "Stag_Ghost_Character",
        "Stag_Highlight_Junk",
        "Stag_Highlight_White",
        "Stag_Paste",
        "Stag_Undo",
        "Stag_Next_Undo"
    };
    
    Theme_Color cols[] = {
        {Stag_Bar},
        {Stag_Bar_Active},
        {Stag_Base},
        {Stag_Pop1},
        {Stag_Pop2},
        {Stag_Back},
        {Stag_Margin},
        {Stag_Margin_Hover},
        {Stag_Margin_Active},
        {Stag_Cursor},
        {Stag_At_Cursor},
        {Stag_Highlight},
        {Stag_At_Highlight},
        {Stag_Mark},
        {Stag_Default},
        {Stag_Comment},
        {Stag_Keyword},
        {Stag_Str_Constant},
        {Stag_Char_Constant},
        {Stag_Int_Constant},
        {Stag_Float_Constant},
        {Stag_Bool_Constant},
        {Stag_Preproc},
        {Stag_Include},
        {Stag_Special_Character},
        {Stag_Ghost_Character},
        {Stag_Highlight_Junk},
        {Stag_Highlight_White},
        {Stag_Paste},
        {Stag_Undo},
        {Stag_Next_Undo}
    };
    
    int count = sizeof(cols) / sizeof(cols[0]);
    get_theme_colors(app, cols, count);
    
    char str[4096]="Theme_Color colours[] = {\n";
    char* str_end = str + 4096 - 10;
    char* str_it = str + strlen(str);
    
    
    
    for(int i=0; i<count && str_it<str_end; i++){
        char tmp[128] = {};
        int len = sprintf(tmp, "%s\t{%s, 0x%x}", i>0? ",\n":"", style_tag_names[i], cols[i].color);
        if(str_it+len < str_end){
            strcpy(str_it, tmp);
            str_it += len;
        }else{
            break;
        }
    }
    
    sprintf(str_it, "\n};\n");
    
    char* msg =  "Copied theme styles to clipboard\n";
    print_message(app,msg, (int)strlen(msg));
    
    clipboard_post(app, 0, str, (int)strlen(str));
}


Theme_Color colours[] = {
    {Stag_Bar, 0xff888888},
    {Stag_Bar_Active, 0xff666666},
    {Stag_Base, 0xff000000},
    {Stag_Pop1, 0xff3c57dc},
    {Stag_Pop2, 0xffff0000},
    {Stag_Back, 0xff161616},
    {Stag_Margin, 0xff181818},
    {Stag_Margin_Hover, 0xff454545},
    {Stag_Margin_Active, 0xff626262},
    {Stag_Cursor, 0xff22ff22},
    {Stag_At_Cursor, 0xff0c0c0c},
    {Stag_Highlight, 0xffddee00},
    {Stag_At_Highlight, 0xffff44dd},
    {Stag_Mark, 0xff33ff33},
    {Stag_Default, 0xffb0b0a0},
    {Stag_Comment, 0xff00ff00},
    {Stag_Keyword, 0xffffffff},
    {Stag_Str_Constant, 0xff33aaff},
    {Stag_Char_Constant, 0xff33aaff},
    {Stag_Int_Constant, 0xff33aaff},
    {Stag_Float_Constant, 0xff33aaff},
    {Stag_Bool_Constant, 0xff33aaff},
    {Stag_Preproc, 0xffc0c0b0},
    {Stag_Include, 0xff33aaff},
    {Stag_Special_Character, 0xffff0000},
    {Stag_Ghost_Character, 0xff4e5e46},
    {Stag_Highlight_Junk, 0xff3a0000},
    {Stag_Highlight_White, 0xff003a3a},
    {Stag_Paste, 0xffddee00},
    {Stag_Undo, 0xff00ddee},
    {Stag_Next_Undo, 0xff000000}
};

HOOK_SIG(cwm_start){
    default_4coder_initialize(app);
    default_4coder_side_by_side_panels(app);
    
    set_theme_colors(app, colours, sizeof(colours) / sizeof(colours[0]) );
    
    // no meaning for return
    return(0);
}
